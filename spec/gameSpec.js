var Game = require('../lib/game');

describe("Game of Life", function () {
  describe("a live cell with less than three live neighbours dies", function () {
    it("kills a live cell if it has no live neighbours", function () {
      game = new Game([
        0,0,0,
        0,1,0,
        0,0,0
      ]);
      expect(game.process()).toEqual([
        0,0,0,
        0,0,0,
        0,0,0
      ]);
    });

    it("kills a live cell if it has no live neighbours", function () {
      game = new Game([
        1,1,1,
        1,1,1,
        1,1,1
      ]);
      expect(game.process()).toEqual([
        0,0,0,
        0,0,0,
        0,0,0
      ]);
    });

    it("kills a live cell if it has no live neighbours", function () {
      game = new Game([
        1,0,0,
        1,1,0,
        1,1,1
      ]);
      expect(game.process()).toEqual([
        1,1,0,
        0,0,1,
        1,0,1
      ]);
    });
  });
});
