module.exports =  function (initialState) {
  this.state = initialState;

  this.process = function () {
    var currentState = this.state.slice(0);
    this.state = tick(currentState);
    return this.state;
  };

  var tick = function (currentState) {
    var width = Math.sqrt(currentState.length)
    var newState = currentState.map(function (cell, i) {
      var neighbours = [];

      neighbours.push( currentState[i - (width+1)] );
      neighbours.push( currentState[i - width] );
      neighbours.push( currentState[i - (width-1)] );

      neighbours.push( currentState[i - 1] );
      neighbours.push( currentState[i + 1] );

      neighbours.push( currentState[i + (width-1)] );
      neighbours.push( currentState[i + width] );
      neighbours.push( currentState[i + (width+1)] );

      var liveCells = neighbours.filter(function (cell) { return cell === 1 });

      if (cell === 1 && (liveCells.length < 2 || liveCells.length > 3)) {
        return 0;
      }
      if (cell === 0 && (liveCells.length === 3)) {
        return 1;
      }
      return cell;
    });

    return newState;
  };
};
