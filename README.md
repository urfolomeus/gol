# Game of life

## Rules

1. Any live cell with fewer than two live neighbours dies, as if caused by under-population.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overcrowding.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

## Thoughts

* We don't need a grid.
* We don't need to know the size of the grid.
* A cell's neighbours are at indices
  * top left      = cell_index - ( Math.sqrt(num_cells) + 1)
  * top middle    = cell_index - Math.sqrt(num_cells)
  * top right     = cell_index - ( Math.sqrt(num_cells) - 1)
  * left          = cell_index - 1
  * right         = cell_index + 1
  * bottom left   = cell_index + ( Math.sqrt(num_cells) - 1)
  * bottom middle = cell_index + Math.sqrt(num_cells)
  * bottom right  = cell_index + ( Math.sqrt(num_cells) + 1)
* If there is not an element at the given index it is treated as a dead cell
* If there is an element at the given index its state is used
